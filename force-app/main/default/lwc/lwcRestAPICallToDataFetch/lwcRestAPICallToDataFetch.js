  
import { LightningElement } from 'lwc';

// The base URL (in this case https://www.googleapis.com/ must be added to the CSP Trusted Sites in Setup) its an open source rest api by ggogle
const QUERY_URL =
    'https://www.googleapis.com/books/v1/volumes?langRestrict=en&q='; // EndPoint

export default class LwcRestAPICallToDataFetch extends LightningElement {
    searchKey = 'Salesforce';
    books;
    error;

    handleSearchKeyChange(event) {
        this.searchKey = event.target.value;
    }

    handleSearchClick() {
        fetch(QUERY_URL + this.searchKey)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response);
                } else {
                    return response.json();
                }
            })
            .then((jsonResponse) => {
                this.books = jsonResponse;
            })
            .catch((error) => {
                this.error = error;
                this.books = undefined;
            });
    }
}